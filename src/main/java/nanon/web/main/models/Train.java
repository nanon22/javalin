package nanon.web.main.models;

import java.time.LocalDateTime;

public class Train {

    String destination;
    int numero;
    LocalDateTime depart;
    LocalDateTime arrivee;

    public Train() { }

    public Train(String dest, int num, LocalDateTime dep, LocalDateTime arr) {
        destination = dest;
        numero = num;
        depart = dep;
        arrivee = arr;
    }

    @Override
    public String toString() {
        String train = "Train " + numero + " à destination de : " + destination + ", départ : " + depart + ", Arrivée : " + arrivee + ".";

        return train;
    }

    public String getDestination() { return  destination; }

    public void setDestination(String Value) {
        destination = Value;
    }

    public int getNumero() { return  numero; }

    public void setNumero(int Valeur) {
        numero = Valeur;
    }

    public LocalDateTime getDepart() { return  depart; }

    public void setDepart(LocalDateTime Valeur) {
        depart = Valeur;
    }

    public LocalDateTime getArrivee() { return  depart; }

    public void setArrivee(LocalDateTime Valeur) {
        arrivee = Valeur;
    }
}
