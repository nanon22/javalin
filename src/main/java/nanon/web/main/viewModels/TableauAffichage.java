package nanon.web.main.viewModels;

import nanon.web.main.models.Train;

import java.util.ArrayList;
import java.util.List;

public class TableauAffichage {

    List<Train> Trains;

    public TableauAffichage() {
        Trains = new ArrayList<>();
    }

    @Override
    public String toString() {
        String intro = Trains.size() + "train(s) à quai : \n";
        for (Train train : Trains) {
            intro += "Train " + train.getNumero() + " à destination de : " + train.getDestination() + ", départ : " + train.getDepart() + ", Arrivée : " + train.getArrivee() + ".\n";
        }

        return intro;
    }

    public void setTrains(List<Train> trains) {
        Trains = trains;
    }

    public void setTrain(Train train) {
        this.Trains.add(train);
    }

    public List<Train> getTrains() {
        return Trains;
    }
}
