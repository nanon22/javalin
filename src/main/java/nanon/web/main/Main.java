package nanon.web.main;

import io.javalin.Javalin;
import nanon.web.main.models.Train;
import nanon.web.main.viewModels.TableauAffichage;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.Month;

public class Main {
    public static void main(String[] args) {

        LocalDateTime aDateTime = LocalDateTime.of(2020, Month.SEPTEMBER, 15, 19, 30, 40);

        LocalDateTime aDateTime2 = LocalDateTime.of(2015, Month.SEPTEMBER, 15, 20, 30, 40);

        Train RER = new Train("Bobigny", 207, aDateTime, aDateTime2);

        Train RER2 = new Train("Marne la vallée", 407, aDateTime, aDateTime2);

        Train RER3 = new Train("Torcy", 607, aDateTime, aDateTime2);

        TableauAffichage tableauAffichage = new TableauAffichage();

        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url("https://api.sncf.com/v1/coverage/sncf/journeys?from=admin:fr:75056&to=admin:fr:69123&datetime=20200915T163833")
                .header("Authorization", "e5ca46af-1f26-40a9-9a04-b761f033113d")
                .build();


        tableauAffichage.setTrain(RER);
        tableauAffichage.setTrain(RER2);
        tableauAffichage.setTrain(RER3);

        Javalin app = Javalin.create().start(7000);

        app.get("/", ctx -> ctx.result("Hello World"));

        app.get("/train", ctx -> ctx.result(tableauAffichage.toString()));

        app.get("/train/:id", ctx -> ctx.result(tableauAffichage.getTrains().get(Integer.parseInt(ctx.pathParam("id"))).toString()));

        app.get("/gare", ctx -> {
            try (Response response = client.newCall(request).execute()) {
                ctx.json(response.body().string());
            }
            catch (IOException e) {
                ctx.result(e.toString());
            }
        });
    }
}
